import enum
from typing import List, Union, Tuple, Any, Dict, Optional, Generator
from .ast import Signal, Value

__all__ = ["Direction", "DIR_NONE", "DIR_FANOUT",
           "DIR_FANIN", "Layout", "Record"]


class Direction(enum.Enum):
    NONE = enum.auto()
    FANOUT = enum.auto()
    FANIN = enum.auto()


DIR_NONE = Direction.NONE
DIR_FANOUT = Direction.FANOUT
DIR_FANIN = Direction.FANIN

# recursive types are not yet supported by mypy, manually recurse a few times
LayoutInputFields0 = Union['Layout',
                           List[Union[Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool],
                                                  Any]],
                                      Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool]],
                                            Direction]]]]

LayoutInputFields1 = Union['Layout',
                           List[Union[Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool],
                                                  LayoutInputFields0]],
                                      Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool]],
                                            Direction]]]]

LayoutInputFields2 = Union['Layout',
                           List[Union[Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool],
                                                  LayoutInputFields1]],
                                      Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool]],
                                            Direction]]]]

LayoutInputFields3 = Union['Layout',
                           List[Union[Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool],
                                                  LayoutInputFields2]],
                                      Tuple[str,
                                            Union[int,
                                                  Tuple[int, bool]],
                                            Direction]]]]

LayoutInputFields = Union['Layout',
                          List[Union[Tuple[str,
                                           Union[int,
                                                 Tuple[int, bool],
                                                 LayoutInputFields3]],
                                     Tuple[str,
                                           Union[int,
                                                 Tuple[int, bool]],
                                           Direction]]]]

LayoutFieldShape = Union[int, Tuple[int, bool], 'Layout']


class Layout:
    fields: Dict[str, Tuple[LayoutFieldShape, Direction]]

    @staticmethod
    def wrap(obj: LayoutInputFields) -> 'Layout':
        ...

    def __init__(self, fields: LayoutInputFields) -> None:
        ...

    def __getitem__(self, name: str) -> Tuple[LayoutFieldShape, Direction]:
        ...

    def __iter__(self) -> Generator[Tuple[str, LayoutFieldShape, Direction],
                                    None,
                                    None]:
        ...


class Record(Value):
    layout: Layout
    fields: Dict[str, Union[Record, Signal]]

    def __init__(self, layout: LayoutInputFields, name: Optional[str] = None):
        ...

    def shape(self) -> Tuple[int, bool]:
        ...

    def __getattr__(self, name: str) -> Union[Record, Signal]:
        ...

    def __getitem__(self,
                    name: Union[slice, int, str]) -> Union[Record, Signal]:
        ...
